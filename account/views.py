from rest_framework import status
from rest_framework.response import Response
from rest_framework.permissions import AllowAny
from rest_framework.generics import CreateAPIView

from django.contrib.auth.models import User
from django.contrib.auth.hashers import make_password

from .serializers import SignUpSerializer, UserSerializer


class SignUpAPIView(CreateAPIView):
    queryset = User.objects.all()
    serializer_class = SignUpSerializer
    permission_classes = (AllowAny,)

    def create(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        data = serializer.data
        data.pop('confirm_password')
        data['password'] = make_password(data['password'])

        user = User.objects.create(**data)

        return Response(data={
            'message': 'Account successfully created',
            },
            status=status.HTTP_201_CREATED)