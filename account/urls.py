from django.urls import path
from . import views as account_views


urlpatterns = [

	path('register/',
		account_views.SignUpAPIView.as_view(), name='register'),

]