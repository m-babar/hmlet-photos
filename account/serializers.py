from rest_framework import routers, serializers, viewsets
from rest_framework.validators import UniqueValidator, ValidationError
from rest_framework import status

from django.contrib.auth.models import User


class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ['id', 'username']


class SignUpSerializer(serializers.Serializer):

    username = serializers.CharField(
        required=True,
        max_length=50,
        )
    password = serializers.CharField(
        required=True,
        max_length=50,
        )
    confirm_password = serializers.CharField(
        required=True,
        max_length=50,
        )

    def validate(self, validated_data):
        password = validated_data['password']
        confirm_password = validated_data['confirm_password']

        if len(password) < 8:
        	raise ValidationError(
        		'Password should be 8 char alpha-neumeric.')

        if password != confirm_password:
        	raise ValidationError(
        		'Password & confirm password must be same.')

        return validated_data