from django.db import models
from django.conf import settings


class PhotoManager(models.Model):

    photo = models.ImageField(upload_to='photos/')
    caption = models.CharField(
        max_length=500, 
        default=' '
        )
    is_draft = models.BooleanField(
        default=False
        )
    creator = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        related_name='photos'
        )
    created_at = models.DateTimeField(
        auto_now_add=True
        )
    updated_at = models.DateTimeField(
        auto_now=True
        )


class PhotoLimitation(models.Model):

    activate_limit = models.BooleanField(
        default=True
        )
    dimension = models.CharField(
        max_length=20,
        default='100*100',
        help_text='Please write dimension limit in W*H format. (Ex: 1920*1720)'
        )
    size = models.IntegerField(
        default='1000000',
        help_text='Please select size in bytes format.'
        )