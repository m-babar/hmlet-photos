from django.contrib import admin

from .models import PhotoManager, PhotoLimitation


admin.site.register(PhotoManager)
admin.site.register(PhotoLimitation)