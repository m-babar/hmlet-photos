from django.urls import path
from .views import PhotoViewSet, GetPhotoLimitationView
from rest_framework import routers


router = routers.SimpleRouter()

router.register(
	r'photo',
	PhotoViewSet, 
	basename='photo')

urlpatterns = [
	path('photo-limit/<int:pk>/',
		GetPhotoLimitationView.as_view(),
	 	name='photo-limit'),

] + router.urls
