from rest_framework import serializers

from .models import PhotoManager, PhotoLimitation
from django.core.files.images import get_image_dimensions


class PhotoSerializer(serializers.ModelSerializer):

    class Meta:
        model = PhotoManager
        fields = ['id', 'photo', 'caption', 'is_draft',
                'creator', 'created_at', 'updated_at']
        read_only_fields = ['id', 'created_at', 
                            'updated_at', 'creator']

    def validate(self, validated_data):

        photo_limit = PhotoLimitation.objects.first()
        if photo_limit.activate_limit: 

            photo = validated_data['photo']
            pxl_w, pxl_h = photo_limit.dimension.split('*')
            photo_size = photo_limit.size
            
            a_width, a_height = get_image_dimensions(photo)
            if(a_width > int(pxl_w) or 
                a_height > int(pxl_h)):
                raise serializers.ValidationError(
                    'Photo Dimension exceeds limit')

            if float(photo.size) > float(photo_size):
                raise serializers.ValidationError(
                    'Photo size exceeds limit.')

        validated_data['creator'] = self.context['request'].user
        return validated_data


class PhotoLimitationSerializer(serializers.ModelSerializer):

    class Meta:
        model = PhotoLimitation
        fields = ['activate_limit', 'dimension', 'size']