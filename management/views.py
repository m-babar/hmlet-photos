from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.filters import OrderingFilter, SearchFilter
from rest_framework.generics import GenericAPIView
from rest_framework.mixins import UpdateModelMixin

from .models import PhotoManager, PhotoLimitation
from .serializers import PhotoSerializer, PhotoLimitationSerializer


class PhotoViewSet(viewsets.ModelViewSet):
    '''
    Photo view help to perform CRUD operation of photo.
    It also perform Ordering, filtering, searching.
    '''
    queryset = PhotoManager.objects.all()
    serializer_class = PhotoSerializer
    permission_classes = (IsAuthenticated,)
    filter_backends = [DjangoFilterBackend, 
        OrderingFilter, SearchFilter]
    filterset_fields = ['is_draft']
    ordering_fields = ['created_at']
    search_fields = ['caption']

    def get_queryset(self):
        return self.queryset.filter(
            creator=self.request.user)


class GetPhotoLimitationView(GenericAPIView, UpdateModelMixin):
    '''
    Photo limitation helps to limit the size & dimension
    of photo while uploading.
    '''
    queryset = PhotoLimitation.objects.all()
    serializer_class = PhotoLimitationSerializer
    permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        serialize = self.serializer_class(self.queryset.first())
        return Response(serialize.data)

    def put(self, request, *args, **kwargs): 
        return self.partial_update(request, *args, **kwargs)