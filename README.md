REGISTER USER:
http://localhost:8000/api/user/register/

GET TOKEN:
http://localhost:8000/api/token/

REFRESH TOKEN:
http://localhost:8000/api/token/refresh/


GET ALL PHOTO OF USER (my draft + my photo)
http://localhost:8000/api/photo/

GET DRAFT PHOTO OF USER:
http://localhost:8000/api/photo/?is_draft=true


GET MY PHOTOS ONLY:
http://localhost:8000/api/photo/?is_draft=false

GET PHOTOS BY CREATED DATE:
For ascending/older first: (created_at)
For descending/recent first: (-created_at)
http://localhost:8000/api/photo/?ordering=created_at

CREATE PHOTO:
POST: http://localhost:8000/api/photo/
data: {
	photo: <IMAGE>
	caption: <string>
	is_draft:<false/true>
}

UPDATE PHOTO FROM DRAFT TO APPROVE:
PATCH: http://localhost:8000/api/photo/<ID>/
data: {
	is_draft:<false/true>
}

UPDATE PHOTO CAPTION:
PATCH: http://localhost:8000/api/photo/<ID>/
data: {
	caption:<"caption data">
}

DELETE PHOTO:
DELETE: http://localhost:8000/api/photo/<ID>/


GET PHOTO LIMIT:
GET: http://localhost:8000/api/photo-limit/1/

UPDATE PHOTO LIMIT:
PUT: http://localhost:8000/api/photo-limit/1/
data:{
	activate_limit:<true/false>
    dimension:<string> #In W*H format
    size:<integer> #In bytes
}
